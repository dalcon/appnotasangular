'use strict';

describe('Service: notasService', function () {

  // load the service's module
  beforeEach(module('pruebapiAngularjsApp'));

  // instantiate service
  var notasService;
  beforeEach(inject(function (_notasService_) {
    notasService = _notasService_;
  }));

  it('should do something', function () {
    expect(!!notasService).toBe(true);
  });

});
