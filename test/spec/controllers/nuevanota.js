'use strict';

describe('Controller: NuevanotactrlCtrl', function () {

  // load the controller's module
  beforeEach(module('pruebapiAngularjsApp'));

  var NuevanotactrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NuevanotactrlCtrl = $controller('NuevanotactrlCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
