'use strict';

/**
 * @ngdoc service
 * @name pruebapiAngularjsApp.notasService
 * @description
 * # notasService
 * Service in the pruebapiAngularjsApp.
 */
angular.module('pruebapiAngularjsApp')
  .service('notasService', ['$resource', function ($resource) {
  	var servicio = $resource('https://pruebaapi.herokuapp.com/api/v1/notes/:id', {id: '@id'});
  	//var servicio = $resource('http://localhost:3000/notes/:id.json', {id: '@id'});

  	this.obtenerNotas = function() {
  		var lista = servicio.query();

  		return lista;
  	}
  }]);
