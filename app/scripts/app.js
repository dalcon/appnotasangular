'use strict';

/**
 * @ngdoc overview
 * @name pruebapiAngularjsApp
 * @description
 * # pruebapiAngularjsApp
 *
 * Main module of the application.
 */
angular
  .module('pruebapiAngularjsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/nuevanota', {
        templateUrl: 'views/nuevaNota.html',
        controller: 'NuevaNotaCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
