'use strict';

/**
 * @ngdoc function
 * @name pruebapiAngularjsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the pruebapiAngularjsApp
 */
angular.module('pruebapiAngularjsApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
