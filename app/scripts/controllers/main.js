'use strict';

/**
 * @ngdoc function
 * @name pruebapiAngularjsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pruebapiAngularjsApp
 */
angular.module('pruebapiAngularjsApp')
  .controller('MainCtrl', ['$scope', 'notasService', function ($scope, notasService) {
    $scope.notas = notasService.obtenerNotas();
  }]);
