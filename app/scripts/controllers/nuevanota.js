'use strict';

/**
 * @ngdoc function
 * @name pruebapiAngularjsApp.controller:NuevanotactrlCtrl
 * @description
 * # NuevanotactrlCtrl
 * Controller of the pruebapiAngularjsApp
 */
angular.module('pruebapiAngularjsApp')
  .controller('NuevaNotaCtrl', ['$scope', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }]);
